﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CameraReticle : MonoBehaviour
{
	public float activationTime = 1.5f;
	public float moveDistanceThreshold = .01f;
	public float guardTime = .888f;
    public float activationThreshold = 0.9f;
	public ReticleDisplay reticleDisplay;
	public EventTrigger.TriggerEvent onGazeRegistered;
	float activateTimer = 0;

    Collider lastColliderHit;
	GameObject lastChartActivated;
	Vector3 lastHit;
	bool wasHitting = false;
	bool chartActive = false;

    AudioSource clickSound;

	int ignoreLayers;

	void Awake(){
		ignoreLayers = (1 << 8) | (1 << 2);
	}
	
	void Start ()
	{
		clickSound = GetComponent<AudioSource> ();
        reticleDisplay.ReticleReadyState();
	}

	void Update ()
	{
		Ray ray = new Ray (transform.position, transform.forward);
//		Debug.DrawRay (transform.position, transform.forward,Color.red, 1f);
		RaycastHit hit = new RaycastHit ();
		if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~ignoreLayers))
        {
            
                if (hit.collider == lastColliderHit && hit.collider.gameObject != lastChartActivated)
                {
                    if (Vector3.Distance(hit.point, lastHit) <= moveDistanceThreshold)
                    {
                        reticleDisplay.ReticleProgressState();
                        activateTimer += Time.deltaTime;
                        if (!chartActive && activateTimer >= activationTime * activationThreshold)
                        {
                            clickSound.Play();
                            StartCoroutine(WaitForSoundAndContinue(hit));
                            chartActive = true;
                        }
                    }
                    else
                    {
                        if (chartActive)
                        {
                            chartActive = false;
                            activateTimer = 0;
                            reticleDisplay.ReticleReadyState();
                        }
                    }
                }
                lastColliderHit = hit.collider;
                lastHit = hit.point;
                wasHitting = true;

            }
            else if (wasHitting)
            {
                wasHitting = false;
                chartActive = false;
                activateTimer = 0;
                reticleDisplay.ReticleReadyState();
            }

        reticleDisplay.ReticleProgress(activateTimer/activationTime);
		
	}

	public void DeactivateLastChart(){
		NotificationCenter.DefaultCenter.PostNotification (this, "chartDeactivated", iTween.Hash ("chart", lastChartActivated));
	}

	IEnumerator WaitForSoundAndContinue (RaycastHit hit)
	{
		yield return new WaitForSeconds (guardTime);
		if (hit.collider.gameObject.CompareTag ("chart")) {
			NotificationCenter.DefaultCenter.PostNotification (this, "chartDeactivated", iTween.Hash ("chart", lastChartActivated));
			NotificationCenter.DefaultCenter.PostNotification (this, "chartActivated", iTween.Hash ("chart", hit.collider.gameObject));
			lastChartActivated = hit.collider.gameObject;
		} else if (hit.collider.gameObject.CompareTag ("button")) {
			NotificationCenter.DefaultCenter.PostNotification (this, "buttonPressed", iTween.Hash ("button", lastColliderHit.gameObject));
		}
		reticleDisplay.ReticleOffState ();
		yield return null;
	}
}
