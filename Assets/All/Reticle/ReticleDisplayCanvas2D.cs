﻿using UnityEngine;
using System.Collections;

public class ReticleDisplayCanvas2D : ReticleDisplay
{

    public GameObject reticleDisplay;
    public GameObject progress;
    public float progressFadeInOutDuration = .333f;

    float f = 0;
    public float rotationTimeStepPercent = .02f;

    public override void ReticleReadyState()
    {
        f = 0;
        reticleDisplay.SetActive(true);
        ProgressBarTransitionOut();
    }

    public override void ReticleProgressState()
    {
        ProgressBarTransitionIn();
    }

    public override void ReticleOffState()
    {
        reticleDisplay.SetActive(false);
        ProgressBarTransitionOut();
    }

    public override void ReticleProgress(float progressPercent)
    {
        if (progressPercent - f * rotationTimeStepPercent >= 0)
        {
            f++;
            progress.transform.rotation = progress.transform.rotation * Quaternion.AngleAxis(-30, new Vector3(0, 0, 1));
        }
    }

    void ProgressBarTransitionIn()
    {
        progress.SetActive(true);
    }

    void ProgressBarTransitionOut()
    {
        progress.SetActive(false);
    }

    IEnumerator FadeAlpha(Material mat, float alpha, float time)
    {
        float _fadeTimer = 0;
        while (_fadeTimer <= time)
        {
            _fadeTimer += Time.deltaTime;
            Color c = mat.color;
            c.a = (_fadeTimer / time) * alpha;
            mat.color = c;
            yield return null;
        }
        yield return null;
    }

}