﻿using UnityEngine;
using System.Collections;

public abstract class ReticleDisplay : MonoBehaviour
{

    public abstract void ReticleReadyState();
    public abstract void ReticleProgressState();
    public abstract void ReticleOffState();
    public abstract void ReticleProgress(float progressPercent);
}
