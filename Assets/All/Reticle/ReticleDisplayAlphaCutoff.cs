﻿using UnityEngine;
using System.Collections;

public class ReticleDisplayAlphaCutoff : ReticleDisplay
{

    public GameObject reticleDisplay;
    public GameObject progress;
    public float progressFadeInOutDuration = .333f;

    Material progressMat;

    void Awake()
    {
        progressMat = progress.GetComponent<Renderer>().material;
    }

    public override void ReticleReadyState()
    {
        reticleDisplay.SetActive(true);
        ProgressBarTransitionOut();
    }

    public override void ReticleProgressState()
    {
        ProgressBarTransitionIn();
    }

    public override void ReticleOffState()
    {
        reticleDisplay.SetActive(false);
        ProgressBarTransitionOut();
    }

    public override void ReticleProgress(float progressPercent)
    {
        progressMat.SetFloat("_Cutoff", Mathf.Lerp(1.2f, 0.05f, progressPercent));
    }

    void ProgressBarTransitionIn()
    {
        progress.SetActive(true);
    }

    void ProgressBarTransitionOut()
    {
        progress.SetActive(false);
    }

    IEnumerator FadeAlpha(Material mat, float alpha, float time)
    {
        float _fadeTimer = 0;
        while (_fadeTimer <= time)
        {
            _fadeTimer += Time.deltaTime;
            Color c = mat.color;
            c.a = (_fadeTimer / time) * alpha;
            mat.color = c;
            yield return null;
        }
        yield return null;
    }


}

