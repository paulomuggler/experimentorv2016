﻿ Shader "Transparent/Cutout/Transparent" {
 Properties {
     _Color ("Main Color", Color) = (1,1,1,1)
     _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     _CutTex ("Cutout (A)", 2D) = "white" {}
     _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    _InnerDiameter ("InnerDiameter", Range(0, 10.0)) = 1.5
    _OuterDiameter ("OuterDiameter", Range(0.00872665, 10.0)) = 2.0
    _DistanceInMeters ("DistanceInMeters", Range(0.0, 100.0)) = 2.0 
 }
 
 SubShader {
     Tags {"Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent"}
     Lighting Off Cull Off ZTest Always ZWrite Off Fog { Mode Off }
     LOD 200
 
 CGPROGRAM
 #pragma surface surf NoLighting alpha vertex:vert

 
 sampler2D _MainTex;
 sampler2D _CutTex;
 fixed4 _Color;
 float _Cutoff;
 uniform float _InnerDiameter;
 uniform float _OuterDiameter;
 uniform float _DistanceInMeters;
 
 struct Input {
     float2 uv_MainTex;
 };

  fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
     {
         fixed4 c;
         c.rgb = s.Albedo; 
         c.a = s.Alpha;
         return c;
     }
 
 void surf (Input IN, inout SurfaceOutput o) {
     fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
     float ca = tex2D(_CutTex, IN.uv_MainTex).a;
     o.Albedo = c.rgb;

     if (ca > _Cutoff)
       o.Alpha = c.a;
     else
       o.Alpha = 0;
 }

 void vert (inout appdata_full i) {
        float scale = lerp(_OuterDiameter, _InnerDiameter, i.vertex.z);

        i.vertex.xyz = float4(i.vertex.x * scale, i.vertex.y * scale, _DistanceInMeters, 1.0);

//        fragmentInput o;
//        o.position = mul (UNITY_MATRIX_MVP, vert_out);
//        return o;
      }
 ENDCG
 }
 
 Fallback "Transparent/VertexLit"
 }