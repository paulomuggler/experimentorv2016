﻿using UnityEngine;
using System.Collections;

public class PanCameraWithHeadRotation : MonoBehaviour {

	public Transform panRoot;

	public float minPanX, minPanY;
	public float maxPanX, maxPanY;

	public float minViewingAngleX, minViewingAngleY;
	public float maxViewingAngleX, maxViewingAngleY;

	public Vector3 rotV;

	void Update () {
		var rot = Cardboard.SDK.HeadPose.Orientation;
		rotV = rot.eulerAngles;
		float panX = rotV.y;
		float panY = rotV.x;

		panX = Extensions.Map (minPanX, maxPanX, minViewingAngleY, maxViewingAngleY, panX);
		panY = Extensions.Map (minPanY, maxPanY, minViewingAngleX, maxViewingAngleX, panY);

		PanCamera (panX, panY);
	}

	void PanCamera(float x, float y){
		Vector3 newPos = new Vector3 (x, y, panRoot.localPosition.z);
		panRoot.localPosition = newPos;
	}
}
