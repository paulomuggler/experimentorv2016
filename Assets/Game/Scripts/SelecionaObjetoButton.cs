﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SelecionaObjetoButton : MonoBehaviour {

	private GameObject objeto;
	private Light luz;
	private string nameObject, nameLight;

	private SelecionaObjeto[] objetosSelecionaveis;
	private SelecionaObjeto[] luzesSelecionaveis;
	private int indexObj, indexLuz;

	ObjetoSelecionado sel;
	private ObjetoSelecionado selecao{
		get{
			sel = FindObjectOfType<ObjetoSelecionado> ();
			return sel;
			}
	}
		
	void Awake () {
		objetosSelecionaveis = FindObjectsOfType<SelecionaObjeto> ().Where (ob => ob.gameObject.GetComponent<ShaderControl> () != null).ToArray ();
		luzesSelecionaveis = FindObjectsOfType<SelecionaObjeto> ().Where (ob => ob.gameObject.GetComponent<LightControl> () != null).ToArray ();

		if (selecao.objeto == null) {
			Objeto (objetosSelecionaveis[indexObj]);
		}
		if (selecao.luz == null) {
			Luz (luzesSelecionaveis[indexLuz]);
		}
	}

	public void Objeto(SelecionaObjeto obj){
		selecao.objeto= obj.gameObject;
		selecao.nameObject = obj.name;
		selecao.SelectionChange ();
	}

	public void Luz(SelecionaObjeto obj){
		selecao.luz= obj.GetComponent<Light>();
		selecao.nameLight = obj.GetComponent<Light>().name;
		selecao.SelectionChange ();
	}

	public void SelecionaProximoObjeto(){
		indexObj++;
		indexObj %= objetosSelecionaveis.Length;
		Objeto (objetosSelecionaveis[indexObj]);
		objetosSelecionaveis [indexObj].SelectionFX ();
	}

	public void SelecionaObjetoAnterior(){
		indexObj--;
		if (indexObj < 0) {
			indexObj = objetosSelecionaveis.Length + indexObj;
		}
		Objeto (objetosSelecionaveis[indexObj]);
		objetosSelecionaveis [indexObj].SelectionFX ();
	}

	public void SelecionaProximaLuz(){
		indexLuz++;
		indexLuz %= luzesSelecionaveis.Length;
		Luz (luzesSelecionaveis[indexLuz]);
		luzesSelecionaveis[indexLuz].SelectionFX ();
	}

	public void SelecionaLuzAnterior(){
		indexLuz--;
		if (indexLuz < 0) {
			indexLuz = luzesSelecionaveis.Length + indexLuz;
		}
		Luz (luzesSelecionaveis[indexLuz]);
		luzesSelecionaveis[indexLuz].SelectionFX ();
	}
}
