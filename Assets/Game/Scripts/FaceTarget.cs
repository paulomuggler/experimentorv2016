﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FaceTarget : MonoBehaviour {

	public Transform target;


	void Update () {
		transform.LookAt (target.position);
	}

}
