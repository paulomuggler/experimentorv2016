﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Security.Cryptography;
using System.Text;

public class Senha : MonoBehaviour {
	public Button confirmarNaoImersivo;
	public Button confirmarImersivo;

	double senhaNaoImersiva;   // e^(mes/dia) * dia^sin(ano em radianos)
	double senhaImersiva;  	// e^(mes/dia) * dia^sin(ano em graus)

	public static string HashSHA1(string value)
	{
		var sha1 = SHA1.Create();
		var inputBytes = Encoding.ASCII.GetBytes(value);
		var hash = sha1.ComputeHash(inputBytes);
		var sb = new StringBuilder();
		for (var i = 0; i < 3; i++)
		{
			sb.Append(hash[i].ToString("X2"));
		}
		return sb.ToString();
	}

	void InicializarSenhas(){
		double dia = DateTime.Now.Day;
		double mes = DateTime.Now.Month;
		double ano = DateTime.Now.Year;

		double e = Math.E;

		senhaNaoImersiva = Math.Pow (e, mes / dia) * Math.Pow (dia, Math.Sin (ano));  					// e^(mes/dia) * dia^sin(ano em radianos)
		senhaImersiva = Math.Pow (e, mes / dia) * Math.Pow (dia, Math.Sin (ano * 2 * Math.PI / 360));  	// e^(mes/dia) * dia^sin(ano em graus)

		while (senhaNaoImersiva < 999) {
			senhaNaoImersiva *= 10;
		}
		senhaNaoImersiva = Math.Floor (senhaNaoImersiva);

		while (senhaImersiva < 999) {
			senhaImersiva *= 10;
		}
		senhaImersiva = Math.Floor (senhaImersiva);
	}

	public void verificarSenha(string texto) {
		/* HASH USANDO SHA1

		string senhaNaoImersiva = HashSHA1(dia.ToString() + mes.ToString() + ano.ToString() + "0");
		string senhaImersiva = HashSHA1 (dia.ToString () + mes.ToString () + ano.ToString () + "1");

		//Debug.Log("Senha Imersiva: " + senhaImersiva);
		//Debug.Log("Senha Nao imersiva: " + senhaNaoImersiva);

		if (texto.Equals (senhaNaoImersiva.ToString())) {
			confirmarNaoImersivo.interactable = true;
			GameObject.Destroy (GameObject.Find("Imersivo"));
		} else {
			confirmarNaoImersivo.interactable = false;
		}

		if (texto.Equals (senhaImersiva.ToString())) {
			confirmarImersivo.interactable = true;
			GameObject.Destroy (GameObject.Find("NaoImersivo"));
		} else {
			confirmarImersivo.interactable = false;
		}
		*/

		//Debug.Log ("Senha Imersiva : " + senhaImersiva.ToString());
		//Debug.Log ("Senha Nao Imersiva: " + senhaNaoImersiva.ToString());
		confirmarImersivo.gameObject.SetActive (true);
		confirmarNaoImersivo.gameObject.SetActive (true);

		confirmarNaoImersivo.interactable = false;
		confirmarImersivo.interactable = false;

		if (texto.Equals (senhaNaoImersiva.ToString())) {
			confirmarNaoImersivo.interactable = true;
			confirmarImersivo.gameObject.SetActive (false);
		} else if (texto.Equals (senhaImersiva.ToString())) {
			confirmarImersivo.interactable = true;
			confirmarNaoImersivo.gameObject.SetActive (false);
		} 
	}

	public void comecarNaoImersivo () {
		Application.LoadLevel ("CenaNaoImersiva");
	}

	public void comecarImersivo () {
		Application.LoadLevel ("CenaImersiva");
	}

	void Awake(){
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}

	// Use this for initialization
	void Start () {
		confirmarNaoImersivo.interactable = false;
		confirmarNaoImersivo.name = "NaoImersiva";
		confirmarImersivo.interactable = false;
		confirmarImersivo.name = "Imersiva";

		InicializarSenhas ();

	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape))
			Application.Quit ();
	}
}