﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DoorLock : MonoBehaviour {

	public AudioSource openSfx, closeSfx, unlockSfx;
	public GameObject endGameCanvas;

	GameObject m_Key;
	Animator animator;
	List<GameObject> keys;
	bool isOpen = false;

	void Start () {
		animator = GetComponent<Animator> ();
		m_Key = GameObject.Find("Key_01");

		PickUpObject[] _keys = FindObjectsOfType<PickUpObject> ();
		m_Key = _keys [Random.Range (0, _keys.Length - 1)].gameObject;
		keys = new List<GameObject> ();
		foreach (PickUpObject po in _keys) {
			keys.Add (po.gameObject);
			po.gameObject.SetActive (false);
		}
//		m_Key = keys [0];
//		m_Key = GameObject.Find ("Key_01");
//		FindObjectOfType<EndGameOnTrigger>().triggerObject = m_Key;
	}

	void OnTriggerEnter(Collider other){
//		Debug.Log ("Trigger");
		if(!isOpen)
			CheckKeyAndOpen (other.gameObject);
	}
//
//	void OnCollisionEnter(Collision collision) {
////		Debug.Log ("Collision");
//		foreach (ContactPoint contact in collision.contacts) {
//			Debug.DrawRay(contact.point, contact.normal, Color.white);
//		}
//	}

	public void CheckKeyAndOpen(GameObject key){
		Debug.Log (key);
		Debug.Log (m_Key);

		if (!key.GetComponent<PickUpObject> ().pickedUp) {
			PlayAudioSource (unlockSfx);
			return;
		}
		
		if (key == m_Key) {
			Debug.Log ("activate");
			PlayAudioSource (unlockSfx);
			animator.SetTrigger ("activate");
			isOpen = true;
			Destroy (key);
			StartCoroutine (OpenEndgameCanvasAfterDelay());
		} else {
			PlayAudioSource (unlockSfx);
			Destroy (key);
		}
	}

	public void PlayOpenSound(){
		PlayAudioSource (openSfx);
	}

	public void PlayCloseSound(){
		PlayAudioSource (closeSfx);
	}

	public void PlayAudioSource(AudioSource s){
		s.Stop ();
		s.Play();
	}

	IEnumerator OpenEndgameCanvasAfterDelay(){
		yield return new WaitForSeconds (2f);
		endGameCanvas.SetActive (true);
	}
		
}
