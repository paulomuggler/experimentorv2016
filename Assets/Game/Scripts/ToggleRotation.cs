﻿using UnityEngine;
using System.Collections;

public class ToggleRotation : MonoBehaviour {

	public float rate = 0.2f;
	public bool x, y, z;
	bool rotate = false;


	void Update () {
		if (rotate) {
			float rateX = rate * System.Convert.ToInt32 (x);
			float rateY = rate * System.Convert.ToInt32 (y);
			float rateZ = rate * System.Convert.ToInt32 (z);

			transform.Rotate(new Vector3(rateX, rateY, rateZ));
		}
	}

	public void Toggle(){
		rotate = !rotate;
	}
}
