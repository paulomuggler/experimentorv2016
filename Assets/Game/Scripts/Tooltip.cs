﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class Tooltip : MonoBehaviour {


	public float ShowTime = 0.33f, HideTime = 2.1f;

	CanvasGroup grp;
	AudioSource popupSfx;

	void Start () {
		grp = GetComponent<CanvasGroup> ();
		popupSfx = GetComponent<AudioSource> ();
	}

	public void Show(){
		LeanTween.cancel (gameObject);
		LeanTween.value (gameObject, 0, 1, ShowTime).setOnUpdate (fadeGroup).setEase (LeanTweenType.easeInCubic).setOnComplete(Hide);
		if (popupSfx.enabled)
			popupSfx.PlayOneShot (popupSfx.clip);
	}

	public void Hide(){
		LeanTween.cancel (gameObject);
		LeanTween.value (gameObject, 1, 0, HideTime).setOnUpdate (fadeGroup).setEase (LeanTweenType.easeInCubic);
	}

	void fadeGroup(float val){
		grp.alpha = val;
	}
}
