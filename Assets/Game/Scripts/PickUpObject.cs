﻿using UnityEngine;
using System.Collections;

public class PickUpObject : MonoBehaviour {

	public static PickUpObject pickedUpObject = null;

	public AudioSource pickupSfx;
	public Transform cursor;
	public Vector3 offsetFromCursor;
	public bool pickedUp = false;

	void Update(){
		if(pickedUp){
			transform.position = cursor.position + cursor.forward * offsetFromCursor.z;
		}
	}

	public void PickOrDrop(){
		if (pickedUp) {
			Drop ();
		} else {
			PickUp ();
		}
	}

	public void PickUp(){

		// drop if picked somtehing up
		if (pickedUpObject != null) {
			pickedUpObject.Drop ();
		}
		pickedUpObject = this;

		pickedUp = true;
//		GetComponent<Collider> ().enabled = false;
		transform.SetParent(cursor);
		pickupSfx.Stop ();
		pickupSfx.Play ();
	}

	public void Drop(){
		pickedUp = false;
		GetComponent<Collider> ().enabled = true;
		transform.SetParent(null);
	}

}
