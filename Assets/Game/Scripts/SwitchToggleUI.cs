﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SwitchToggleUI : MonoBehaviour {

	Toggle tg;
	public Image switchGraphics;
	public Vector3 switchPositionOn, switchPositionOff;

	void Awake () {
		tg = GetComponent<Toggle> ();
		tg.onValueChanged.AddListener (Toggle);
		Toggle (tg.isOn);
	}

	public void Toggle(bool state){
		Vector3 position;
		if (state) {
			position = switchPositionOn;
		} else {
			position = switchPositionOff;
		}
		switchGraphics.rectTransform.localPosition = position;
	}

}
