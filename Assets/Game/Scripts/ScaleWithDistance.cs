﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScaleWithDistance : MonoBehaviour
{

	public Transform target;

	public float maxDistance;
	public float minScale, maxScale;

	public Vector3 baseScale;

	void Update ()
	{
		float d = Vector3.Distance (target.position, transform.position);
		float f = d / (maxDistance);
//		float s = f * (maxScale);
		float s = f;
		s = Mathf.Clamp (s, minScale, maxScale);
//		Debug.Log (d);
//		Debug.Log (f);
//		Debug.Log (s);
		transform.localScale = baseScale * s;
	}
}
