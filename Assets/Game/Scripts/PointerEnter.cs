﻿using UnityEngine;
using System.Collections;

public class PointerEnter : MonoBehaviour {

	private bool active,cresce;
	float size, maxSize,minSize;
	float passo;
	private Vector3 escala;
	private ParticleSystem Particulas ;
	private Material cor;


	// Use this for initialization
	void Awake () {
		active = false;
		cresce = true;
		maxSize = 1.4f;
		minSize = 1f;
		size = 1f;
		passo = 0.01f;
		escala = this.transform.localScale;
		Particulas = this.GetComponent<ParticleSystem> ();
		cor = this.GetComponent<Renderer>().material;



	}
	
	// Update is called once per frame
	void Update () {
		if (active) {

			Particulas.startColor = cor.color;

			if (size < maxSize && cresce) {
				size = size + passo;
				this.transform.localScale = escala * size;

			} 
			else if (size >= maxSize && cresce) {
				cresce = false;
				size = size - passo;
				this.transform.localScale = escala * size;

				}
			else if (size > minSize && !cresce) {
				
				size = size - passo;
				this.transform.localScale = escala * size;

			}
			else  {
				cresce = true;
				size = size + passo;
				this.transform.localScale = escala * size;

			}

		}
	}

	public void Entrou(){
		active = true;
		Particulas.Play (true);
	}

	public void Saiu(){
		active = false;
		this.transform.localScale = escala;
		size = 1f;
		cresce = true;

		Particulas.Stop (true);
	}

}
