﻿using UnityEngine;
using System.Collections;

public class SelecionaObjeto : MonoBehaviour {

	public PointerEnter selecaoFx;
	public float selectionFXDuration = 1.333f;

	private GameObject objeto;
	private Light luz;
	private string nameObject, nameLight;

	ObjetoSelecionado sel;
	private ObjetoSelecionado selecao{
		get{
			sel = FindObjectOfType<ObjetoSelecionado> ();
			return sel;
			}
	}
		
	void Start () {
		if (selecao.objeto == null) {
			Objeto ();
		}
		if (selecao.luz == null) {
			Luz ();
		}
	}

	public void SelectionFX(){
		StartCoroutine (runSelectionFX (selectionFXDuration));
	}

	public void Objeto(){
		selecao.objeto= this.gameObject;
		selecao.nameObject = this.name;
		SelectionFX ();
		selecao.SelectionChange ();
	}

	public void Luz(){
		selecao.luz= this.GetComponent<Light>();
		selecao.nameLight = this.GetComponent<Light>().name;
		SelectionFX ();
		selecao.SelectionChange ();
	}

	IEnumerator runSelectionFX(float seconds){
		selecaoFx.Entrou ();
		yield return new WaitForSeconds (seconds);
		selecaoFx.Saiu ();
	}
}
