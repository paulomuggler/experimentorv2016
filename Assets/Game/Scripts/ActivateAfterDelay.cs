﻿using UnityEngine;
using System.Collections;

public class ActivateAfterDelay : MonoBehaviour {

	public float delay = 2f;
	public GameObject activateGameObject;

	void Start () {
		StartCoroutine (WaitForDelayAndActivate ());
	}
	
	IEnumerator WaitForDelayAndActivate(){
		yield return new WaitForSeconds (delay);
		activateGameObject.SetActive (true);
	}

}
