﻿using UnityEngine;
using System.Collections;

public class ToggleLightControlMenu : MonoBehaviour {

	public CanvasGroup lightControlCanvasGroup;

	bool enabled = false;

	void Start(){
		SetEnabled (enabled);
	}

	public void Toggle(){
		enabled = !enabled;
		SetEnabled (enabled);
	}

	public void Enable(){
		enabled = true;
		SetEnabled (enabled);
	}

	public void Disable(){
		enabled = false;
		SetEnabled (enabled);
	}

	void SetEnabled(bool enabled){
		lightControlCanvasGroup.interactable = enabled;
		lightControlCanvasGroup.blocksRaycasts = enabled;
		lightControlCanvasGroup.alpha = System.Convert.ToInt32 (enabled);
	}
}
