﻿using UnityEngine;
using System.Collections;

public class LightControl : MonoBehaviour {
    private Light sceneLight;
    public float r, g, b;
	public float lightIntensity;

	public ShaderControl followMaterial;

	public float Attenuation {
		get {
			return 1f / lightIntensity;
		}
	}

    // altera a intensidade das componentes da fonte de luz (Ici)
    public void SetRed(float x)
    {
        r = x;
    }
    public void SetGreen(float x)
    {
        g = x;
    }
    public void SetBlue(float x)
    {
        b = x;
    }

	public void SetAttenuation(float x)
	{
		lightIntensity = 1f/x;
	}

	public void SetIntensity(float x)
	{
		lightIntensity = x;
	}

	bool _on = true;
	public bool IsOn {
		get {
			return _on;
		}
	}

	float prevIntensity = 1;
	public void Toggle(){
		Toggle (_on);
	}

	public void SetOn(bool on){
		Toggle (!on); // weird, I know
	}

	public void Toggle(bool on){
		if (on) {
			Off ();
		} else {
			On ();
		}
	}

	void On(){
		_on = true;
		GetComponent<Light> ().enabled = true;
//		lightIntensity = prevIntensity;
	}

	void Off(){
		_on = false;
//		prevIntensity = lightIntensity;
//		lightIntensity = 0f;
		GetComponent<Light> ().enabled = false;
	}

    // falta definir qual propriedade no Unity corresponde ao Fat (fator de atenuacao) e o n (imperfeicao da reflexao)
	// R: Fat corresponde à propriedade intensity da fonte de luz. n - imperfeição da reflexão - é uma propriedade do material/shader

    void Start () {
        sceneLight = GetComponent<Light>();

		r = sceneLight.color.r;
		g = sceneLight.color.g;
		b = sceneLight.color.b;
		lightIntensity = sceneLight.intensity;
        
	}
	
	void Update () {
        sceneLight.color = new Color(r, g, b);
		sceneLight.intensity = lightIntensity;

		followMaterial.setRed (r);
		followMaterial.setGreen (g);
		followMaterial.setBlue (b);

//		followMaterial.setEmissiveness (lightIntensity); //TODO
	}
}
