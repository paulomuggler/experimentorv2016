﻿using UnityEngine;
using System.Collections;

public class ObjetoSelecionado : MonoBehaviour {
	public string nameObject,nameLight;

	[HideInInspector] public GameObject objeto;
	[HideInInspector] public Light luz;

	public void SelectionChange(){
		SelectableLightControlPanel lightPanel = FindObjectOfType<SelectableLightControlPanel> ();
		lightPanel.SelectionChanged (luz.gameObject);

		SelectableObjectPanel objectPanel = FindObjectOfType<SelectableObjectPanel> ();
		objectPanel.SelectionChanged (objeto.gameObject);
	}

}
