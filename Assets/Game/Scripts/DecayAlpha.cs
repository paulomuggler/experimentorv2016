﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class DecayAlpha : MonoBehaviour {

	public float DecayRate = 0.2f;

	CanvasGroup grp;

	void Start () {
		grp = GetComponent<CanvasGroup> ();
	}

	void Update () {
		grp.alpha = Mathf.Clamp (grp.alpha * DecayRate - Time.deltaTime, 0, 1);
	}
}
