﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class SelectableLightControlPanel : MonoBehaviour {

	LightControl selectedLc;
	Text objectName;
	Slider red, green, blue, intensity;
	Toggle toggleSwitch;
	ResultingColorDisplay colorDisplay;

	void Awake(){
		initializeFields ();
	}

	void initializeFields(){
		red = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Red").First ();
		green = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Green").First ();
		blue = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Blue").First ();
		intensity = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Intensity").First ();
		toggleSwitch = gameObject.GetComponentInChildren<Toggle> ();
		colorDisplay = gameObject.GetComponentInChildren<ResultingColorDisplay> ();
		colorDisplay.initializeFields ();
		objectName = gameObject.GetComponentsInChildren<Text> ().Where (cmp => cmp.gameObject.transform.parent.name == "Header").First ();
	}
		
	public void SelectionChanged(GameObject selection){
		selectedLc = selection.GetComponent<LightControl> ();
		SetCallbacks ();
		SetValues ();
	}

	void SetValues(){
		objectName.text = selectedLc.gameObject.name;
		red.value = selectedLc.r;
		green.value = selectedLc.g;
		blue.value = selectedLc.b;
		intensity.value = selectedLc.lightIntensity;
		toggleSwitch.isOn = selectedLc.IsOn;
	}

	void SetCallbacks () {

		red.onValueChanged.RemoveAllListeners ();
		green.onValueChanged.RemoveAllListeners ();
		blue.onValueChanged.RemoveAllListeners ();
		intensity.onValueChanged.RemoveAllListeners ();

		red.onValueChanged.AddListener (selectedLc.SetRed);
		green.onValueChanged.AddListener (selectedLc.SetGreen);
		blue.onValueChanged.AddListener (selectedLc.SetBlue);
		intensity.onValueChanged.AddListener (selectedLc.SetIntensity);

		toggleSwitch.onValueChanged.RemoveAllListeners ();
		toggleSwitch.onValueChanged.AddListener (selectedLc.SetOn);
		toggleSwitch.onValueChanged.AddListener (toggleSwitch.GetComponent<SwitchToggleUI> ().Toggle);

		colorDisplay.AddValueChangeLIsteners ();
	}
}
