﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LightRay : MonoBehaviour {
	public Light fonteLuz;
	public GameObject Cursor;
	public float width;
	public Text beta;

	private bool raios;

	Vector3 olhos;
	Vector3 ponto;
	Vector3 luz;

	Vector3 target;

	public void setLightRays() {
		olhos = Camera.main.transform.position;
		ponto = Cursor.transform.position;
		luz = fonteLuz.transform.position;

		Ray rayOlhos = new Ray (olhos, ponto - olhos);
		Ray rayLuz = new Ray (luz, ponto - luz);
		RaycastHit hit;

		olhos = rayOlhos.GetPoint (0.5f);

		if (Physics.Raycast (rayOlhos, out hit)) {
			raios = true;

			target = hit.point;
			Ray rayNormal = new Ray (target, hit.normal);

			Debug.DrawRay (luz, target - luz);

			//drawIncoming (luz, target, Color.green);
			//drawNormal (target, rayNormal.GetPoint (0.5f), Color.red);
			//drawReflected (target, Vector3.Reflect (target - luz, hit.normal), Color.green);

			Ray rayIncidente = new Ray (luz, target - luz);

			if (Physics.Raycast (rayIncidente, out hit)) {
				target = hit.point;
				rayNormal = new Ray (target, hit.normal);

				drawIncoming (luz, target, Color.green);
				drawNormal (target, rayNormal.GetPoint (0.5f), Color.red);
				//drawReflected (target, Vector3.Reflect (target - luz, hit.normal), Color.green);

				if (Physics.Raycast (new Ray(target, Vector3.Reflect(target - luz, hit.normal)), out hit)) {
					Vector3 t = hit.point;

					drawReflected (target, t, Color.green);
				}
			}

		}
	}

	void Start() {
		width = 0.005f;
		raios = false;
	}

	// Update is called once per frame
	void Update () {
		if (raios) {
			olhos = Camera.main.transform.position;
			luz = fonteLuz.transform.position;
			Ray rayOlhos = new Ray (olhos, target - olhos);
			olhos = rayOlhos.GetPoint (0.5f);

			drawLine (olhos, target, Color.yellow);

			beta.text = (Vector3.Angle (target - olhos, target - luz)).ToString();
		}
	}

	void drawReflected (Vector3 start, Vector3 end, Color color) {
        if (raios) {
        	GameObject.Destroy (GameObject.Find("myLineReflected"));
        }

        GameObject myLineReflected = new GameObject();
        myLineReflected.name = "myLineReflected";

        myLineReflected.transform.position = start;
        myLineReflected.AddComponent<LineRenderer> ();

        LineRenderer lr = myLineReflected.GetComponent<LineRenderer> ();

        lr.material = new Material (Shader.Find ("Standard"));
		lr.material.SetColor ("_Color", color);
        //lr.SetColors (color,color);
        lr.SetWidth (width, width);
        lr.SetPosition (0, start);
        lr.SetPosition (1, end);
    }

	void drawLine (Vector3 start, Vector3 end, Color color) {
		if (raios) {
			GameObject.Destroy (GameObject.Find("myLine"));
		}

		GameObject myLine = new GameObject();
		myLine.name = "myLine";

		myLine.transform.position = start;
		myLine.AddComponent<LineRenderer> ();

		LineRenderer lr = myLine.GetComponent<LineRenderer> ();

		lr.material = new Material (Shader.Find ("Standard"));
		lr.material.SetColor ("_Color", color);
		//lr.SetColors (color,color);
		lr.SetWidth (width, width);
		lr.SetPosition (0, start);
		lr.SetPosition (1, end);
	}

	void drawIncoming (Vector3 start, Vector3 end, Color color) {
		if (raios) {
			GameObject.Destroy (GameObject.Find("myLine2"));
		}

		GameObject myLine2 = new GameObject ();
		myLine2.name = "myLine2";

		myLine2.transform.position = start;
		myLine2.AddComponent<LineRenderer> ();

		LineRenderer lr = myLine2.GetComponent<LineRenderer> ();

		lr.material = new Material (Shader.Find ("Standard"));
		lr.material.SetColor ("_Color", color);
		//lr.SetColors (color, color);
		lr.SetWidth (width, width);
		lr.SetPosition (0, start);
		lr.SetPosition (1, end);
	}

	void drawNormal (Vector3 start, Vector3 end, Color color) {
		if (raios) {
			GameObject.Destroy (GameObject.Find("myLine3"));
		}

		GameObject myLine3 = new GameObject ();
		myLine3.name = "myLine3";

		myLine3.transform.position = start;
		myLine3.AddComponent<LineRenderer> ();

		LineRenderer lr = myLine3.GetComponent<LineRenderer> ();

		lr.material = new Material (Shader.Find ("Standard"));
		lr.material.SetColor ("_Color", color);
		//lr.SetColors (color,color);
		lr.SetWidth (width, width);
		lr.SetPosition (0, start);
		lr.SetPosition (1, end);
	}
}
