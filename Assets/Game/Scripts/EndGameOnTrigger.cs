﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndGameOnTrigger : MonoBehaviour {

	public GameObject triggerObject;

	void OnTriggerEnter(Collider other){
		Debug.Log ("endgame trigger");
		Debug.Log (other.gameObject);
		Debug.Log (triggerObject);
		if (other.gameObject == triggerObject) {
			foreach (AudioSource s in GetComponents<AudioSource>()) {
				s.Play ();
			}
			GetComponentInChildren<CanvasGroup> ().alpha = 1.0f;
			Destroy (other.gameObject);
		}
	}

	public void ResetGame(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
