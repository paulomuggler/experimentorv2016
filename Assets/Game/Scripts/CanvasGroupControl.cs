﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupControl : MonoBehaviour {

	public float fadeInTime = 0.666f, fadeoutTime = 1.222f;

	LTDescr m_TweenAlpha;

	CanvasGroup m_Group;
	CanvasGroup Group {
		get {
			if (m_Group == null) {
				m_Group = GetComponent<CanvasGroup> ();
			}
			return m_Group;
		}
	}

//	void Start(){
//		Hide ();
//	}

	public void Show(){
//		if (m_TweenAlpha != null) {
//			LeanTween.cancel (m_TweenAlpha.id);
//		}
		m_TweenAlpha = LeanTween.value (gameObject, 0, 1, fadeInTime).setOnUpdate (UpdateAlpha).setEase (LeanTweenType.easeOutExpo);
		Group.interactable = true;
		Group.blocksRaycasts = true;
		Group.interactable = true;
	}

	public void Hide(){
//		if (m_TweenAlpha != null) {
//			LeanTween.cancel (m_TweenAlpha.id);
//		}
		m_TweenAlpha = LeanTween.value (gameObject, 1, 0, fadeoutTime).setOnUpdate (UpdateAlpha).setEase (LeanTweenType.easeInExpo);
		Group.interactable = false;
		Group.blocksRaycasts = false;
		Group.interactable = false;
	}

	void UpdateAlpha(float a){
		Group.alpha = a;
	}

}
