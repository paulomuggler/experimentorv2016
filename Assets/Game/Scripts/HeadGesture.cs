﻿using UnityEngine;
using System.Collections;

public class HeadGesture : MonoBehaviour {
	public bool isFacingDown = false;
	public float angulo;

	// Use this for initialization
	void Start () {
		angulo = 60.0f;	
	}
	
	// Update is called once per frame
	void Update () {
		isFacingDown = DetectFacingDown ();
	}

	private bool DetectFacingDown () {
		return (CameraAngleFromGround () < angulo);
	}

	private float CameraAngleFromGround () {
		return Vector3.Angle (Vector3.down, Camera.main.transform.rotation * Vector3.forward);
	}
}
