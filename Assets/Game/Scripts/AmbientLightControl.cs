﻿using UnityEngine;
using System.Collections;

public class AmbientLightControl : MonoBehaviour {
    public float ar, ag, ab;
    public float ambientReflection;
	public float ambientIntensity;

	public float Attenuation {
		get {
			return 1f / ambientIntensity;
		}
	}


    // altera o coeficiente de reflexao da luz ambiente (Ka)
    public void setAmbientReflection(float x)
    {
        ambientReflection = x;
    }

    // altera a intensidade das componentes da luz ambiente (Iac)
    public void setAmbientRed(float x)
    {
        ar = x;
    }
    public void setAmbientGreen(float x)
    {
        ag = x;
    }
    public void setAmbientBlue(float x)
    {
        ab = x;
    }

	public void SetAmbientAttenuation(float x)
	{
		ambientIntensity = 1f/x;
	}	

	public void SetAmbientIntensity(float x)
	{
		ambientIntensity = x;
	}	

    // falta definir qual propriedade no Unity corresponde ao Fat (fator de atenuacao) e o n (imperfeicao da reflexao)
	// R: Fat corresponde à propriedade intensity da fonte de luz. n - imperfeição da reflexão - é uma propriedade do material/shader

    void Start () {
		ar = RenderSettings.ambientLight.r;
		ag = RenderSettings.ambientLight.g;
		ab = RenderSettings.ambientLight.b;
		ambientIntensity = RenderSettings.ambientIntensity;
		ambientReflection = RenderSettings.reflectionIntensity;
//        ambientReflection = 1f;
//		ambientIntensity = 1f;
	}
	
	void Update () {
        RenderSettings.reflectionIntensity = ambientReflection;
        RenderSettings.ambientLight = new Color(ar, ag, ab);
		RenderSettings.ambientSkyColor = new Color(ar, ag, ab);
		RenderSettings.ambientIntensity = ambientIntensity;
	}
}
