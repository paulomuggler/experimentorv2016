﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class ResultingColorDisplay : MonoBehaviour {

	public Slider red, green, blue;

	Image display;

	void Awake(){
//		initializeFields ();
	}

	public void initializeFields(){
		display = GetComponent<Image> ();
	
		red = transform.parent.gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Red").First ();
		green = transform.parent.gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Green").First ();
		blue = transform.parent.gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Blue").First ();
		AddValueChangeLIsteners ();
		SetResultingColor (0f);
	}

	public void AddValueChangeLIsteners(){
		red.onValueChanged.AddListener(SetResultingColor);
		green.onValueChanged.AddListener(SetResultingColor);
		blue.onValueChanged.AddListener(SetResultingColor);
	}

	void SetResultingColor(float v){
		display.color = new Color (red.value, green.value, blue.value);
	}
}
