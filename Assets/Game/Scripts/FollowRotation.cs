﻿using UnityEngine;
using System.Collections;
using System;

public class FollowRotation : MonoBehaviour {

	public Transform target;
	public bool x, y, z;

	void Update () {

		Vector3 t = target.localRotation.eulerAngles;
		Vector3 o = transform.localRotation.eulerAngles;

		float rX = x ? t.x : o.x;
		float rY = y ? t.y : o.y;
		float rZ = z ? t.z : o.z;

		Vector3 rotEuler = new Vector3 (rX, rY, rZ);
		transform.localRotation = Quaternion.Euler (rotEuler);

//		transform.localRotation = target.localRotation;
	}
}
