﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(LineRenderer))]
public class ObservingAngleDisplay : MonoBehaviour {

	public Transform observer;
	public Light sceneLight;

	Light[] sceneLights;

	LineRenderer lr;

	void Awake(){
		lr = GetComponent<LineRenderer> ();
		sceneLights = FindObjectsOfType<Light> ();
	}

	public void DrawReferenceVectors(Vector3 observedPoint){
		lr.SetVertexCount (3);
		lr.SetPosition (0, sceneLight.transform.position);
		lr.SetPosition (1, observedPoint);
		lr.SetPosition (2, observer.position);
	}
}
