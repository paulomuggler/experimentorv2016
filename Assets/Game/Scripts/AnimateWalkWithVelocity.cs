﻿using UnityEngine;
using System.Collections;

public class AnimateWalkWithVelocity : MonoBehaviour {

	public float dampTime = 3;

	Animator animator;
	public NavMeshAgent agent;

	void Start () {
		animator = GetComponent<Animator> ();
//		agent = GetComponent<NavMeshAgent> ();
	}

	void Update () {
		Vector3 velocity = agent.velocity;
		float z = velocity.z;
		float x = velocity.x;
		Vector3 horizontalvelocity = new Vector3(x,0,z);
		float horizontalspeed = horizontalvelocity.magnitude;

		if (horizontalspeed > float.Epsilon) {
			transform.rotation = Quaternion.LookRotation (horizontalvelocity, Vector3.up);
		}

		animator.SetFloat("speed",horizontalspeed,dampTime, 0.2f);
	}
}
