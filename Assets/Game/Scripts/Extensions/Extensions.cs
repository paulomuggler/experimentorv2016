﻿
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Extensions
{
	public static List<MethodInfo> GetMethods(this MonoBehaviour mb, Type returnType, Type[] paramTypes, BindingFlags flags)
	{
		return mb.GetType().GetMethods(flags)
			.Where(m => m.ReturnType == returnType)
				.Select(m => new { m, Params = m.GetParameters() })
				.Where(x =>
				       {
					return paramTypes == null ? // in case we want no params
						x.Params.Length == 0 :
							x.Params.Length == paramTypes.Length &&
							x.Params.Select(p => p.ParameterType).ToArray().SequenceEqual(paramTypes);
				})
				.Select(x => x.m)
				.ToList();
	}
	
	public static List<MethodInfo> GetMethods(this GameObject go, Type returnType, Type[] paramTypes, BindingFlags flags)
	{
		var mbs = go.GetComponents<MonoBehaviour>();
		List<MethodInfo> list = new List<MethodInfo>();
		
		foreach (var mb in mbs) {
			list.AddRange(mb.GetMethods(returnType, paramTypes, flags));
		}
		
		return list;
	}

	public static T GetComponentInHierarchy<T>(GameObject go) where T : Component{
		T component = null;
		component = go.GetComponent<T> ();
		if (component == null) {
			component = go.GetComponentInChildren<T> ();
		}
		if (component == null) {
			component = go.GetComponentInParent<T> ();
		}
		return component;
	}

	
	public static T[] GetComponentsInHierarchy<T>(GameObject go) where T : Component{
		T[] components = go.GetComponents<T>().Union(go.GetComponentsInChildren<T> ()).Union(go.GetComponentsInParent<T> ()).ToArray();
		return components;
	}

	public static T RandomElementFromArray<T>(T[] ary){
		return ary[UnityEngine.Random.Range(0, ary.Length)];
	}

	/// <summary>
	/// Get the screen rect bounds of this button. corners is working storage for 4 points, it is set to the screen space coordinates
	/// or use the returned Rect which finds the min/max
	/// </summary>
	public static Rect GetScreenRect(RectTransform rectTransform, Vector3[] corners) {
		rectTransform.GetWorldCorners(corners);
		
		float xMin = float.PositiveInfinity, xMax = float.NegativeInfinity, yMin = float.PositiveInfinity, yMax = float.NegativeInfinity;
		for (int i = 0; i < 4; ++i) {
			// For Canvas mode Screen Space - Overlay there is no Camera; best solution I've found
			// is to use RectTransformUtility.WorldToScreenPoint) with a null camera.
			Vector3 screenCoord = RectTransformUtility.WorldToScreenPoint(null, corners[i]);
			if (screenCoord.x < xMin) xMin = screenCoord.x;
			if (screenCoord.x > xMax) xMax = screenCoord.x;
			if (screenCoord.y < yMin) yMin = screenCoord.y;
			if (screenCoord.y > yMax) yMax = screenCoord.y;
			corners[i] = screenCoord;
		}
		Rect result = new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
		return result;
	}

	public static float Map(float mapFrom, float mapTo, float mappedFrom, float mappedTo, float value){
		if(value <= mappedFrom){
			return mapFrom;
		}else if(value >= mappedTo){
			return mapTo;
		}else{
			return (mapTo - mapFrom) * ((value - mappedFrom) / (mappedTo - mappedFrom)) + mapFrom;
		}
	}

	public static float LinearStep(float from, float to, float timer, float endTime){
		float t = timer / endTime;
		return Mathf.Lerp(from, to, t);
	}

	public static float EaseOutStep(float from, float to, float timer, float endTime){
		float t = timer / endTime;
		t = Mathf.Sin(t * Mathf.PI * 0.5f);
		return Mathf.Lerp(from, to, t);
	}

	public static float EaseInStep(float from, float to, float timer, float endTime){
		float t = timer / endTime;
		t = 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
		return Mathf.Lerp(from, to, t);
	}

	public static float ExponentialStep(float from, float to, float timer, float endTime){
		float t = timer / endTime;
		t = t * t;
		return Mathf.Lerp(from, to, t);
	}
	public static float SmoothStep(float from, float to, float timer, float endTime){
		float t = timer / endTime;
		t = t * t * (3f - 2f * t);
		return Mathf.Lerp(from, to, t);
	}

	public static float SmootherStep(float from, float to, float timer, float endTime){
		float t = timer / endTime;
		t = t * t * t * (t * (6f * t - 15f) + 10f);
		return Mathf.Lerp(from, to, t);
	}

	public static bool IsBetween(float v, float min, float max){
		return v <= max && v >= min;
	}
}