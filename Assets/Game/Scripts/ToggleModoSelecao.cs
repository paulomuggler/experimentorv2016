﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ToggleModoSelecao : MonoBehaviour {

	public bool on;
//	private List<GraphicRaycaster> raycaster; 
//	private List<Collider> objetos;

	public GraphicRaycaster[] raycaster;
	public Collider[] objetos;

	// Use this for initialization
	void Start () {
//		on = true;
		SetOn(on);
//		raycaster = new List<GraphicRaycaster> ();
//
//		//SettingsCanvas
//		raycaster.Add(GameObject.Find("Cena_Aula_Experimental/console1/ui/SettingsCanvas").GetComponent<GraphicRaycaster>());
//
//		//AmbientLightSettingsCanvas
//		raycaster.Add(GameObject.Find("Cena_Aula_Experimental/console1/ui/AmbientLightSettingsCanvas").GetComponent<GraphicRaycaster>());
//
//		//ShaderSettingsCanvas
//		raycaster.Add(GameObject.Find("Cena_Aula_Experimental/console1/ui/ShaderSettingsCanvas").GetComponent<GraphicRaycaster>());
//
//
//		objetos = new List<Collider> ();
//
//		//utah-teapot
//		objetos.Add(GameObject.Find("Cena_Aula_Experimental/props/utah-teapot").GetComponent<Collider>());
//
//		//vase2
//		objetos.Add(GameObject.Find("Cena_Aula_Experimental/props/vase2").GetComponent<Collider>());
//
//		//vase1_low
//		objetos.Add(GameObject.Find("Cena_Aula_Experimental/props/vase1_low").GetComponent<Collider>());
//
//		//light_wall_fixture (1)/Sphere
//		objetos.Add(GameObject.Find("Cena_Aula_Experimental/lights/light_wall_fixture (1)/Sphere").GetComponent<Collider>());
//
//		//light_wall_fixture (2)/Sphere (1)
//		objetos.Add(GameObject.Find("Cena_Aula_Experimental/lights/light_wall_fixture (2)/Sphere (1)").GetComponent<Collider>());
//
//		//light_ceiling_fixture/Sphere
//		objetos.Add(GameObject.Find("Cena_Aula_Experimental/lights/light_ceiling_fixture/Sphere").GetComponent<Collider>());

	}


	public void Toggle(){
		if (on) {
			Off ();
		} else {
			On ();
		}
	}

	public void SetOn(bool _on){
		on = !_on;
		Toggle ();
	}

	void On(){
		on = true;

		for (int a = 0; a < raycaster.Length; a++) {
			raycaster [a].enabled= true;
		}

		for (int a = 0; a < objetos.Length; a++) {
			objetos [a].enabled= true;
		}
	}

	void Off(){
		on = false;

		for (int a = 0; a < raycaster.Length; a++) {
			raycaster [a].enabled= false;

		}

		for (int a = 0; a < objetos.Length; a++) {
			objetos [a].enabled= false;
		}

	}


}
