﻿using UnityEngine;
using System.Collections;

public class Tablet : MonoBehaviour {
	public GameObject dashboard;
	private GameObject head;
	private GameObject character;
	private HeadGesture gesture;

	private Vector3 rotation;
	private Vector3 originalPosition;
	private bool toggle;
	private bool down;
	private bool entered;

	public float distanceZ = 0.7f;
	public float distanceY = -1.0f;

	// Use this for initialization
	void Start () {
		down = false;
		toggle = false;
		entered = false;
		dashboard = GameObject.Find ("Tablet");
		character = GameObject.Find ("MyCharacter_Unity");
		head = GameObject.Find ("Head");
		gesture = GetComponent<HeadGesture> ();
		dashboard.transform.position = head.transform.position + character.transform.rotation*new Vector3(0.0f, 0.0f, distanceZ) + new Vector3(0.0f, distanceY, 0.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (gesture.isFacingDown && !toggle) {
			rotation = getRotation ();
			down = true;
			toggle = true;
		}

		originalPosition = head.transform.position + character.transform.rotation*new Vector3(0.0f, 0.0f, distanceZ) + new Vector3(0.0f, distanceY, 0.0f);

		if (down) {
			if (dashboard.transform.position.y <= 1.6f)
				dashboard.transform.position += new Vector3 (0.0f, 1.0f * Time.deltaTime, 0.0f);
			dashboard.transform.eulerAngles = rotation + new Vector3 (-25.0f, 0.0f, 0.0f);
		} else if (!down) {
			dashboard.transform.eulerAngles = Camera.main.transform.eulerAngles + new Vector3 (15.0f, 0.0f, 0.0f);

			if (dashboard.transform.position.y > originalPosition.y)
				dashboard.transform.position -= new Vector3 (0.0f, 1.0f * Time.deltaTime, 0.0f);
			
			if (dashboard.transform.position.x > originalPosition.x)
				dashboard.transform.position -= new Vector3 (1.0f * Time.deltaTime, 0.0f, 0.0f);
			if (dashboard.transform.position.x < originalPosition.x)
				dashboard.transform.position += new Vector3 (1.0f * Time.deltaTime, 0.0f, 0.0f);
			
			if (dashboard.transform.position.z > originalPosition.z)
				dashboard.transform.position -= new Vector3 (0.0f, 0.0f, 1.0f * Time.deltaTime);
			if (dashboard.transform.position.z < originalPosition.z)
				dashboard.transform.position += new Vector3 (0.0f, 0.0f, 1.0f * Time.deltaTime);
		}
	}

	public Vector3 getRotation () {
		return dashboard.transform.eulerAngles;
	}

	// Associar com o Event Trigger de quando o cursor sai do tablet
	public void ExitTablet () {
		if (entered) {
			toggle = false;
			down = false;
			entered = false;
		}
	}

	// Associar com o Event Trigger de quando o cursor entra no tablet
	public void EnterTablet() {
		entered = true;
	}
}
