﻿using UnityEngine;
using System.Collections;

public class ShaderControl : MonoBehaviour {
	
    public Renderer shader;
	public MeshFilter mesh;
	public Mesh meshWithInterpolatedNormals, meshWithImportedNormals;
    public float g;
    public float m;
    public float specRed, specGreen, specBlue;
	public float red, green, blue;

	public bool Interpolate{
		get{
//			return mesh.mesh == meshWithInterpolatedNormals;
			return interpolateNormals;
		}
		set{
			ToggleMeshType (value);
		}
	}

	public bool IsOn {
		get {
			return shader.enabled = true;
		}
		set{
			shader.enabled = value;
		}
	}

	public static ShaderControl[] allShaderControls = null;
    
	void Start () {
        shader = GetComponent<Renderer>();
		mesh = GetComponent<MeshFilter> ();
		shader.material.EnableKeyword("_Color");
        shader.material.EnableKeyword("_Glossiness");
        shader.material.EnableKeyword("_Metallic");
        shader.material.EnableKeyword("_SpecColor");
        
        g = 0.5f;
        m = 0.0f;
		red = green = blue = 1f;
        specRed = specGreen = specBlue = 1f;

		if (allShaderControls == null) {
			allShaderControls = FindObjectsOfType<ShaderControl> ();
		}

		if (meshWithInterpolatedNormals == null) {
			meshWithInterpolatedNormals = mesh.mesh;
		}

		if (meshWithImportedNormals == null) {
			meshWithImportedNormals = mesh.mesh;
		}

		setMeshInterpolatedNormals ();

	}

	bool oncePerFrame = false;
	void LateUpdate(){
		oncePerFrame = false;
	}

	void CallForAllShaders(string methodName, object value){
		if (!oncePerFrame) {
			oncePerFrame = true;
			foreach (ShaderControl sc in allShaderControls) {
				sc.SendMessage (methodName, value);
			}
		}
	}
    	
    // altera a propriedade Smoothness do shader (Ke)
    public void setSmoothness(float x)
    {
        g = x;
//		CallForAllShaders ("setSmoothness", x);
    }

    // altera a propriedade Metallic do shader 
    public void setMetallic(float x)
    {
        m = x;
//		CallForAllShaders ("setMetallic", x);
    }

	// alteram a intesidade da cor difusa (Cec)
	public void setRed(float x)
	{
		red = x;
//		CallForAllShaders ("setRed", x);
	}
	public void setGreen(float x)
	{
		green = x;
//		CallForAllShaders ("setGreen", x);
	}
	public void setBlue(float x)
	{
		blue = x;
//		CallForAllShaders ("setBlue", x);
	}
    // alteram a intesidade da cor especular (Cec)
    public void setSpecRed(float x)
    {
        specRed = x;
//		CallForAllShaders ("setSpecRed", x);
    }
	public void setSpecGreen(float x)
    {
        specGreen = x;
//		CallForAllShaders ("setSpecGreen", x);
    }
	public void setSpecBlue(float x)
    {
        specBlue = x;
//		CallForAllShaders ("setSpecBlue", x);
    }
		
	public void ToggleMeshType(bool interpolated){
		if(interpolated){
			setMeshInterpolatedNormals();
		}else{
			setMeshImportedNormals();
		}
//		CallForAllShaders ("ToggleMeshType", interpolated);
	}

	bool interpolateNormals = true;
	public void setMeshInterpolatedNormals(){
		interpolateNormals = true;
		mesh.mesh = meshWithInterpolatedNormals;
	}

	public void setMeshImportedNormals(){
		interpolateNormals = false;
		mesh.mesh = meshWithImportedNormals;
	}

    // falta definir qual propriedade do Unity corresponde ao Cdc (componente da cor difusa) e Kd (coeficiente de reflexao da luz 
    // difusa) na fórmula
	// R: Cdc -> _Color, Kd -> _Metallic


    void Update () {
        shader.material.SetFloat("_Glossiness", g);
        shader.material.SetFloat("_Metallic", m);
		shader.material.SetColor("_Color", new Color(red, green, blue));
        shader.material.SetColor("_SpecColor", new Color(specRed, specGreen, specBlue));
    }
}
