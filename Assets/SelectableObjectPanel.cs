﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class SelectableObjectPanel : MonoBehaviour {

	ShaderControl selectedObj;
	Text objectName;
	Slider red, green, blue, glossiness, metallic;
	Toggle toggleSwitch;
	Toggle normalsSwitch;
	ResultingColorDisplay colorDisplay;

	void Awake(){
		initializeFields ();
	}

	void initializeFields(){
		red = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Red").First ();
		green = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Green").First ();
		blue = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Blue").First ();

		glossiness = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Glossiness").First ();
		metallic = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Metallic").First ();

		toggleSwitch = gameObject.GetComponentsInChildren<Toggle> ().Where (cmp => cmp.gameObject.name == "ToggleSwitch").First ();
		normalsSwitch = gameObject.GetComponentsInChildren<Toggle> ().Where (cmp => cmp.gameObject.name == "Interpolate").First ();

		colorDisplay = gameObject.GetComponentInChildren<ResultingColorDisplay> ();
		colorDisplay.initializeFields ();
		objectName = gameObject.GetComponentsInChildren<Text> ().Where (cmp => cmp.gameObject.transform.parent.name == "Header").First ();
	}

	public void SelectionChanged(GameObject selection){
		selectedObj = selection.GetComponent<ShaderControl> ();
		SetCallbacks ();
		SetValues ();
	}

	void SetValues(){
		objectName.text = selectedObj.gameObject.name;

		red.value = selectedObj.red;
		green.value = selectedObj.green;
		blue.value = selectedObj.blue;

		glossiness.value = selectedObj.g;
		metallic.value = selectedObj.m;

		toggleSwitch.isOn = selectedObj.IsOn;
		normalsSwitch.isOn = selectedObj.Interpolate;
	}

	void SetCallbacks () {

		red.onValueChanged.RemoveAllListeners ();
		green.onValueChanged.RemoveAllListeners ();
		blue.onValueChanged.RemoveAllListeners ();

		glossiness.onValueChanged.RemoveAllListeners ();
		metallic.onValueChanged.RemoveAllListeners ();

		red.onValueChanged.AddListener (selectedObj.setRed);
		green.onValueChanged.AddListener (selectedObj.setGreen);
		blue.onValueChanged.AddListener (selectedObj.setBlue);

		metallic.onValueChanged.AddListener (selectedObj.setMetallic);
		glossiness.onValueChanged.AddListener (selectedObj.setSmoothness);
	
		toggleSwitch.onValueChanged.RemoveAllListeners ();
		toggleSwitch.onValueChanged.AddListener ((bool b) => {selectedObj.IsOn = b;});
		toggleSwitch.onValueChanged.AddListener (toggleSwitch.GetComponent<SwitchToggleUI> ().Toggle);

		normalsSwitch.onValueChanged.RemoveAllListeners ();
		normalsSwitch.onValueChanged.AddListener ((bool b) => {selectedObj.Interpolate = b;});
		normalsSwitch.onValueChanged.AddListener (normalsSwitch.GetComponent<SwitchToggleUI> ().Toggle);

		colorDisplay.AddValueChangeLIsteners ();
	}
}
