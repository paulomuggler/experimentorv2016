﻿/* © 2015 Studio Pepwuper http://www.pepwuper.com */

using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour {

	public GameObject Cursor; // Google Cardboard SDK: Cursor / GazePointer from CardboardMain Prefab
	private Vector3 goal;
	private NavMeshAgent agent;
	
	void Start() {
		this.agent = GetComponent<NavMeshAgent>();
		this.goal = new Vector3(0f, 0f, 0f);
	}

	bool walking = false;
	Coroutine autoWalkCoroutine;
	public void ToggleAutowalking(){
		walking = !walking;
		if (walking) {
			autoWalkCoroutine = StartCoroutine (UpdateNavGoal ());
		} else {
			StopCoroutine (autoWalkCoroutine);
		}
	}

	IEnumerator UpdateNavGoal(){
		while (true) {
			
			this.goal = transform.position + transform.forward * 0.1f;
//			Debug.Log ("autowalking, goal: " + this.goal);
			MoveToDestination ();
			yield return null;
		}
	}
	
	//Set navigation destination to the position of the cursor
	//Ex. Call this from an event trigger on the floor object
	public void SetDestinationToCursor() {
		Vector3 origin = Camera.main.transform.position;
		Vector3 direction = Cursor.transform.position - Camera.main.transform.position;
		Ray ray = new Ray (origin, direction);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit)) {
			this.goal = hit.point;
		}else{
			this.goal = Cursor.transform.position;
		}

		MoveToDestination();
	}
	
	void MoveToDestination(){
		this.agent.destination = goal; 
	}
}