﻿/* © 2015 Studio Pepwuper http://www.pepwuper.com */

using UnityEngine;
using System.Collections;

public class FollowOther : MonoBehaviour {

	public Vector3 offset;

	public GameObject target;
	private Vector3 targetPosition;
	
	void LateUpdate () {
		targetPosition = target.transform.position;
//		Vector3 newPos = new Vector3(playerPosition.x, transform.position.y, playerPosition.z); // not changing Y value
		Vector3 newPos = new Vector3(targetPosition.x, targetPosition.y, targetPosition.z) + offset;// changing Y value
		transform.position = newPos;
	}
}
