﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class AmbientLightUIInit : MonoBehaviour {

	AmbientLightControl ambientLightControl;
	Slider red, green, blue, intensity;
	ResultingColorDisplay colorDisplay;
	SwitchToggleUI switchToggle;

	void Awake(){
		initializeFields ();
		SetCallbacks ();
		SetValues ();
	}

	void initializeFields(){
		ambientLightControl = FindObjectOfType<AmbientLightControl> ();
		red = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Red").First ();
		green = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Green").First ();
		blue = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Blue").First ();
		intensity = gameObject.GetComponentsInChildren<Slider> ().Where (cmp => cmp.gameObject.transform.parent.name == "Intensity").First ();
		colorDisplay = gameObject.GetComponentInChildren<ResultingColorDisplay> ();
		colorDisplay.initializeFields ();
	}
	
	void SetValues(){
		red.value = ambientLightControl.ar;
		green.value = ambientLightControl.ag;
		blue.value = ambientLightControl.ab;
		intensity.value = ambientLightControl.ambientIntensity;
	}

	void SetCallbacks () {

		red.onValueChanged.RemoveAllListeners ();
		green.onValueChanged.RemoveAllListeners ();
		blue.onValueChanged.RemoveAllListeners ();
		intensity.onValueChanged.RemoveAllListeners ();

		red.onValueChanged.AddListener (ambientLightControl.setAmbientRed);
		green.onValueChanged.AddListener (ambientLightControl.setAmbientGreen);
		blue.onValueChanged.AddListener (ambientLightControl.setAmbientBlue);
		intensity.onValueChanged.AddListener (ambientLightControl.SetAmbientIntensity);

		colorDisplay.AddValueChangeLIsteners ();
	}
}
